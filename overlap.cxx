#include <iostream>
#include <string>
#include <map>
#include <set>
#include <array>
#include <vector>
#include "tinyxml2.h"

using namespace tinyxml2;

struct SectorNumberConverter {

    std::pair<int,int> barrel_global2local(int sector){
        auto inoct_sector = ((sector + 2) % 4);
        auto mioct_number = ((sector + 2) / 4) % 8;
        return std::make_pair(inoct_sector,mioct_number);
    }
    
    int barrel_local2global(int number,int mioct){
        return ((30 + 4 * mioct)  + number) % 32;
    }

    std::pair<int,int>  endcap_global2local(int sector){
        auto inoct_sector = ((sector + 1) % 6);
        auto mioct_number = ((sector + 1) / 6) % 8;
        return std::make_pair(inoct_sector,mioct_number);

    }
    
    int endcap_local2global(int number,int mioct){
        return ((47 + 6 * mioct)  + number) % 48;
    }
    std::pair<int,int> forward_global2local(int sector){
        auto inoct_sector = (sector % 3);
        auto mioct_number = (sector / 3) % 8;
        return std::make_pair(inoct_sector,mioct_number);

    }
    
    int forward_local2global(int number,int mioct){
        return ((0 + 3 * mioct)  + number) % 24;
    }
};

class MyVisitor : public XMLVisitor {

    int active_mioct = -1;
    int active_side = -1;
    int sec_left = -1;
    int sec_right = -1;
    std::map<int,std::set<std::string> > global_pairs;

    std::array<std::map<std::string,std::vector<std::string>>,2> lhs_index;
    std::array<std::map<std::string,std::vector<std::string>>,2> rhs_index;

    std::string make_key(std::string prefix, int global_sec, int roi){
        prefix += std::to_string(global_sec) + "_" + std::to_string(roi);
        return prefix;
    }

    std::string make_pair(std::string lhs, std::string rhs){
        return lhs + ":" + rhs;
    }
    virtual bool VisitEnter( const XMLElement& element, const XMLAttribute* firstAttribute )	{
        std::cout << "yup: " << element.GetLineNum() << ": " << element.Name()  << std::endl;

        SectorNumberConverter sc;
        std::map<std::string,std::pair<int,int> > or_regions = {
            {"BB1LUT",  {2,3} },
            {"BB2LUT",  {3,4} },
            {"BE11LUT", {1,1} },
            {"BE12LUT", {1,2} },
            {"BE22LUT", {2,2} },
            {"BE23LUT", {2,3} },
            {"BE34LUT", {3,4} },
            {"BE35LUT", {3,5} },
            {"BE45LUT", {4,5} },
            {"BE46LUT", {4,6} },
            {"EE1LUT",  {1,2} },
            {"EE2LUT",  {2,3} },
            {"EE3LUT",  {3,4} },
            {"EE4LUT",  {4,5} },
            {"EE5LUT",  {5,6} },
            {"EF21LUT", {2,1} },
            {"EF31LUT", {3,1} },
            {"EF42LUT", {4,2} },
            {"EF52LUT", {5,2} },
            {"EF63LUT", {6,3} },
            {"FF1LUT",  {1,2} },
            {"FF2LUT",  {2,3 }}

        };

        if (std::string("Mioct").compare(element.Name()) == 0){
            active_mioct = element.IntAttribute("IDs") % 8;
            active_side = element.IntAttribute("IDs") / 8;
            std::cout << "a mioct!" << active_mioct << " " << active_side << std::endl;
        }
        else if (std::string(element.Name()).find("LUT") != std::string::npos && std::string("MUCTPI_LUT").compare(element.Name())){
            std::cout << "a LUT!!" << std::endl;
            auto secs = or_regions[element.Name()];
            sec_left  = secs.first;
            sec_right = secs.second;
            if(std::string(element.Name()).find("BB") == 0){
                sec_left  =  sc.barrel_local2global(sec_left-1,active_mioct);
                sec_right =  sc.barrel_local2global(sec_right-1,active_mioct);
            }
            else if(std::string(element.Name()).find("BE") == 0){
                sec_left  =  sc.barrel_local2global(sec_left-1,active_mioct);
                sec_right =  sc.endcap_local2global(sec_right-1,active_mioct);
            }
            else if(std::string(element.Name()).find("EE") == 0){
                sec_left  =  sc.endcap_local2global(sec_left-1,active_mioct);
                sec_right =  sc.endcap_local2global(sec_right-1,active_mioct);
            }
            else if(std::string(element.Name()).find("EF") == 0){
                sec_left  =  sc.endcap_local2global(sec_left-1,active_mioct);
                sec_right =  sc.forward_local2global(sec_right-1,active_mioct);
            }
            else if(std::string(element.Name()).find("FF") == 0){
                sec_left  =  sc.forward_local2global(sec_left-1,active_mioct);
                sec_right =  sc.forward_local2global(sec_right-1,active_mioct);
            }
        }
        else if (std::string("BBElement").compare(element.Name()) == 0){
            auto roi1 = element.IntAttribute("RoI1");
            auto roi2 = element.IntAttribute("RoI2");
            auto lhs_key = make_key("B",sec_left,roi1);
            auto rhs_key = make_key("B",sec_right,roi2);
            auto region = make_pair(lhs_key,rhs_key);
            global_pairs[active_side].insert(region);
            std::cout << "region: " << region << std::endl;
        }
        else if (std::string("BEElement").compare(element.Name()) == 0){
            auto roi1 = element.IntAttribute("BRoI");
            auto roi2 = element.IntAttribute("ERoI");
            auto lhs_key = make_key("B",sec_left,roi1);
            auto rhs_key = make_key("E",sec_right,roi2);
            auto region = make_pair(lhs_key,rhs_key);
            global_pairs[active_side].insert(region);
            std::cout << "region: " << region << std::endl;
        }
        else if (std::string("EEElement").compare(element.Name()) == 0){
            auto roi1 = element.IntAttribute("RoI1");
            auto roi2 = element.IntAttribute("RoI2");
            auto lhs_key = make_key("E",sec_left,roi1);
            auto rhs_key = make_key("E",sec_right,roi2);
            auto region = make_pair(lhs_key,rhs_key);
            global_pairs[active_side].insert(region);
            std::cout << "region: " << region << std::endl;
        }
        else if (std::string("EFElement").compare(element.Name()) == 0){
            auto roi1 = element.IntAttribute("ERoI");
            auto roi2 = element.IntAttribute("FRoI");
            auto lhs_key = make_key("E",sec_left,roi1);
            auto rhs_key = make_key("F",sec_right,roi2);
            auto region = make_pair(lhs_key,rhs_key);
            global_pairs[active_side].insert(region);
            std::cout << "region: " << region << std::endl;
        }
        else if (std::string("FFElement").compare(element.Name()) == 0){
            auto roi1 = element.IntAttribute("RoI1");
            auto roi2 = element.IntAttribute("RoI2");
            auto lhs_key = make_key("F",sec_left,roi1);
            auto rhs_key = make_key("F",sec_right,roi2);
            auto region = make_pair(lhs_key,rhs_key);
            global_pairs[active_side].insert(region);
            std::cout << "region: " << region << std::endl;
        }

        return true;
    }

    public:
    void create_indices(){
        for(auto side_regions : global_pairs ){
            for(auto region : side_regions.second){
                auto split = region.find(':');
                auto left = region.substr(0,split);
                auto right = region.substr(split+1,std::string::npos);
                std::cout << side_regions.first << " " <<  left << " " << right << std::endl;
                lhs_index[side_regions.first][left].push_back(right);
                rhs_index[side_regions.first][right].push_back(left);
            }
        }
    }

    std::vector<std::string> get_lhs_keys(std::string dettype, int roi, int sector){
        std::vector<std::string>  r;
        r.push_back(dettype + std::to_string(sector) + "_" + std::to_string(roi));
        return r;   
    }

    std::vector<std::string> get_rhs_keys(std::string dettype, int roi, int sector){
        std::vector<std::string>  r;
        r.push_back(dettype + std::to_string(sector) + "_" + std::to_string(roi));
        return r;   
    }

    std::vector<std::string> relevant_regions(int side, const std::string& dettype, int roi, int sector){
        std::vector<std::string>  r;
        for(auto key : get_lhs_keys(dettype,roi,sector)){
            std::cout << "lhs key: " << key  << std::endl;
            auto x = lhs_index[side].find(key);
            if(x != lhs_index[side].end()){
                for(auto rr : lhs_index[side][key]){
                    r.push_back(make_pair(key,rr));
                }
            }
        }
        for(auto key : get_rhs_keys(dettype,roi,sector)){
            std::cout << "rhs key: " << key  << std::endl;
            auto x = rhs_index[side].find(key);
            if(x != rhs_index[side].end()){
                for(auto rr : rhs_index[side][key]){
                    r.push_back(make_pair(rr,key));
                }
            }
        }
        return r;
    }
};

struct Candidate{
    Candidate(int side, std::string dettype,  int sector, int roi):side(side),dettype(dettype), roi(roi), sector(sector){}
    int side;
    std::string dettype;
    int roi;
    int sector;

};

int main(){

    XMLDocument doc;
    doc.LoadFile("data10_7TeV.periodI.physics_Muons.MuCTPI_LUT.AllOverlaps_composedEF.v002.xml");

    MyVisitor v;
    doc.Accept(&v);
    v.create_indices();

    std::cout << "hello" << std::endl;

    SectorNumberConverter sc;
    auto x = sc.endcap_global2local(42);
    std::cout << x.first << " " << x.second << std::endl; 

    auto y = sc.endcap_local2global(x.first,x.second);
    std::cout << y << std::endl;

    std::vector<Candidate> cands = {
        Candidate(0,"B",21,13),
        Candidate(0,"B",21,14),
        Candidate(0,"E",33,0),
        Candidate(0,"E",33,4),
    };

    int count = 0;
    std::array<
        std::map<std::string,std::vector<int> >,
        2
    > buckets;
    for(auto c : cands){
        for(auto rr : v.relevant_regions(c.side,c.dettype,c.roi,c.sector)){
            std::cout << "candidate " << count << " region: " << rr << std::endl;
            buckets[c.side][rr].push_back(count);
        }
        count++;
    }

    for(auto side_olaps : buckets ){
        for(auto olap : side_olaps){
            std::cout << "olap: " << olap.first << "  -> size: " << olap.second.size() << " cands: [";
            for(auto cc : olap.second){
                std::cout << cc << ", ";
            }
            std::cout << "]" << std::endl;
        }
    }

    return 0;
}