class SectorNumberConverter(object):
    @staticmethod
    def barrel_global2local(sector):
        inoct_sector = ((sector + 2) % 4)
        mioct_number = ((sector + 2) // 4) % 8
        return inoct_sector,mioct_number
    
    @staticmethod
    def barrel_local2global(number,mioct):
        return ((30 + 4 * mioct)  + number) % 32
    
    @staticmethod
    def endcap_global2local(sector):
        inoct_sector = ((sector + 1) % 6)
        mioct_number = ((sector + 1) // 6) % 8
        return inoct_sector,mioct_number

    @staticmethod
    def endcap_local2global(number,mioct):
        return ((47 + 6 * mioct)  + number) % 48

    @staticmethod
    def forward_global2local(sector):
        inoct_sector = (sector % 3)
        mioct_number = (sector // 3) % 8
        return inoct_sector,mioct_number

    @staticmethod
    def forward_local2global(number,mioct):
        return ((0 + 3 * mioct)  + number) % 24


class OverlapConfig(object):
    def __init__(self,filepath):
        import xml.etree.ElementTree as ET
        tree = ET.parse(open(filepath))

        self.lut_tags = []
        self.attribs = []
        self.global_pairs = {0: [], 1: []}

        self.or_regions = {
            'BB1LUT':  [1,2],
            'BB2LUT':  [3,4],
            'BE11LUT': [1,1],
            'BE12LUT': [1,2],
            'BE22LUT': [2,2],
            'BE23LUT': [2,3],
            'BE34LUT': [3,4],
            'BE35LUT': [3,5],
            'BE45LUT': [4,5],
            'BE46LUT': [4,6],
            'EE1LUT':  [1,2],
            'EE2LUT':  [2,3],
            'EE3LUT':  [3,4],
            'EE4LUT':  [4,5],
            'EE5LUT':  [5,6],
            'EF21LUT': [2,1],
            'EF31LUT': [3,1],
            'EF42LUT': [4,2],
            'EF52LUT': [5,2],
            'EF63LUT': [6,3],
            'FF1LUT':  [1,2],
            'FF2LUT':  [2,3]
        }
        
        for x in tree.iter():
            if x.tag == 'Mioct':
                active_mioct = int(x.attrib["IDs"] ) % 8    
                active_side =  int(x.attrib["IDs"] ) // 8
                print(active_side)
            if 'LUT' in x.tag and x.tag!='MUCTPI_LUT':
                detmap = {
                    'B': SectorNumberConverter.barrel_local2global,
                    'E': SectorNumberConverter.endcap_local2global,
                    'F': SectorNumberConverter.forward_local2global
                }
                active_lut = x.tag
                sec_left,sec_right = self.or_regions[active_lut]
                sectype_left, sectype_right = active_lut[:2]
                lhs_sec_global = detmap[sectype_left](sec_left-1,active_mioct)
                rhs_sec_global = detmap[sectype_right](sec_right-1,active_mioct)
                self.lut_tags.append(active_lut)
            if 'Element' in x.tag:
                self.attribs.append(','.join(list(x.attrib.keys())))
            if 'BBEl' in x.tag:
                lhs_key = self.make_key('B',lhs_sec_global,x.attrib["RoI1"])
                rhs_key = self.make_key('B',rhs_sec_global,x.attrib["RoI2"])
                name = self.make_pair_name(lhs_key,rhs_key)
                self.global_pairs[active_side].append(name)

            if 'BEEl' in x.tag:
                lhs_key = self.make_key('B',lhs_sec_global,x.attrib["BRoI"])
                rhs_key = self.make_key('E',rhs_sec_global,x.attrib["ERoI"])
                name = self.make_pair_name(lhs_key,rhs_key)
                self.global_pairs[active_side].append(name)

            if 'EEEl' in x.tag:
                lhs_key = self.make_key('E',lhs_sec_global,x.attrib["RoI1"])
                rhs_key = self.make_key('E',rhs_sec_global,x.attrib["RoI2"])
                name = self.make_pair_name(lhs_key,rhs_key)
                self.global_pairs[active_side].append(name)


            if 'EFEl' in x.tag:
                lhs_key = self.make_key('E',lhs_sec_global,x.attrib["ERoI"])
                rhs_key = self.make_key('F',rhs_sec_global,x.attrib["FRoI"])
                name = self.make_pair_name(lhs_key,rhs_key)
                self.global_pairs[active_side].append(name)

            if 'FFEl' in x.tag:
                lhs_key = self.make_key('F',lhs_sec_global,x.attrib["RoI1"])
                rhs_key = self.make_key('F',rhs_sec_global,x.attrib["RoI2"])
                name = self.make_pair_name(lhs_key,rhs_key)
                self.global_pairs[active_side].append(name)

        self.global_pairs = {k:sorted(set(v)) for k,v in self.global_pairs.items()}
        self.indices = {0: self.getindices(side = 0), 1: self.getindices(side = 1)}


    def make_key(self,dettype,sector,roi):
        return '{}{}_{}'.format(dettype,sector,roi)


    def make_pair_name(self,lhs,rhs):
        return  '{}:{}'.format(lhs,rhs)

    def getindices(self,side):
        lhs_index = {}
        rhs_index = {}
        for x in self.global_pairs[side]:
            x1,x2 = x.split(':')
            lhs_index.setdefault(x1,[]).append(x2)
            rhs_index.setdefault(x2,[]).append(x1)
        return lhs_index,rhs_index

    def get_lhs_keys(self,roi):
        detmap = {2: 'B', 1: 'F', 0: 'E'}
        key = '{}{}_{}'.format(detmap[roi.dettype],int(roi.sector),int(roi.roiid))
        return [key]

    def get_rhs_keys(self,roi):
        detmap = {2: 'B', 1: 'F', 0: 'E'}
        key = '{}{}_{}'.format(detmap[roi.dettype],int(roi.sector),int(roi.roiid))
        return [key]
        
    def yield_overlaps(self,roi):
        for lhs_key in self.get_lhs_keys(roi):
            rhs_keys = self.indices[roi.side][0].get(lhs_key,[])
            for rhs_key in rhs_keys: #loop over OR regions with cand in lhs
                region = self.make_pair_name(lhs_key,rhs_key)
                yield region
        for rhs_key in self.get_rhs_keys(roi):
            lhs_keys = self.indices[roi.side][1].get(rhs_key,[])
            for lhs_key in lhs_keys: #loop over OR regions with cand in rhs
                region = self.make_pair_name(lhs_key,rhs_key)
                yield region


import pandas as pd
import numpy as np 
class Geometry(object):

    def __init__(self,ef_file,b_file):
        d = np.loadtxt(ef_file)
        df_endcap = pd.DataFrame(d[d[:,1]==0],
                columns = ['side','dettype','sector','roiid','etamin','etamax','phimin','phimax']
        )


        df_forward = pd.DataFrame(d[d[:,1]==1],
                columns = ['side','dettype','sector','roiid','etamin','etamax','phimin','phimax']
        )


        d = np.loadtxt(b_file) 
        df_barrel = pd.DataFrame(d,
                columns = ['side','sector','roiid','etamin','etamax','phimin','phimax']
        )
        df_barrel = self.process_barrel_roi(df_barrel)
        df_endcap = self.process_endcap_roi(df_endcap)
        df_forward = self.process_forward_roi(df_forward)
        self.df = pd.concat([df_barrel,df_endcap,df_forward],ignore_index=True)


    def process_df(self,df):
        df['is_overlap'] = False
        df['eta'] = df.etamin + (df.etamax - df.etamin)/2.
        df['phi'] = df.phimin + (df.phimax - df.phimin)/2.
        return df

    def process_barrel_roi(self,df):
        df = self.process_df(df)
        inoct_sector,mioct_number = SectorNumberConverter.barrel_global2local(df.sector)
        df['inoct_sector'] = inoct_sector
        df['mioct_number'] = mioct_number
        df['dettype'] = 2*np.ones_like(df.roiid)
        df['side'] = 1-df['side'] #fix inconsistency
        return df

    def process_endcap_roi(self,df):
        df = self.process_df(df)
        inoct_sector,mioct_number = SectorNumberConverter.endcap_global2local(df.sector)
        df['inoct_sector'] = inoct_sector
        df['mioct_number'] = mioct_number
        return df

    def process_forward_roi(self,df):
        df = self.process_df(df)
        inoct_sector,mioct_number = SectorNumberConverter.forward_global2local(df.sector)
        df['inoct_sector'] = inoct_sector
        df['mioct_number'] = mioct_number
        return df


def process_candidates(oc,df):
    buckets = {0: {}, 1: {}}
    for i,x in df.iterrows(): #loop over candidates
        detmap = {2: 'B', 1: 'F', 0: 'E'}
        for overlap in oc.yield_overlaps(x):
            buckets[x.side].setdefault(overlap,[]).append(i)
    with_olap = [[k,v] for side,side_bucket in buckets.items() for k,v in side_bucket.items() if len(v) > 1]
    print(len(with_olap),'pairs found')
    return np.concatenate([c for _,c in with_olap])
